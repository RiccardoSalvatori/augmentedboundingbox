﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.AugmentedBoundingBox
{
    abstract class BoundingBoxDecorator : IAugmentedBoundingBox
    {
        protected IAugmentedBoundingBox augBB;
        public BoundingBoxDecorator(IAugmentedBoundingBox augBB)
        {
            this.augBB = augBB;
        }

        public void DecreaseScale()
        {
            this.augBB.DecreaseScale();
        }

        public void IncreaseScale()
        {
            this.augBB.IncreaseScale();
        }

        public BoundingBoxData GetSerializable()
        {
            return this.augBB.GetSerializable();
        }

        public GameObject GetObject()
        {
            return this.augBB.GetObject();
        }

        public void AddType(BoundingBoxType type)
        {
            this.augBB.AddType(type);
        }
    }
}
