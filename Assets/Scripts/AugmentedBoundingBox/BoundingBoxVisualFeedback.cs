﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta.HandInput;
using Meta;
using UnityEngine.UI;

/// <summary>
/// Handles visual  feedback of the interacting bounding box.
/// This script is placed in the bounding box prefab.
/// </summary>
public class BoundingBoxVisualFeedback : MonoBehaviour, IGazeStartEvent, IGazeEndEvent {


    [SerializeField]
    private Material _default;
    [SerializeField]
    private Material _near;
    [SerializeField]
    private Material _grab;

    private HandsProvider handsProvider;
    private bool isGazed;


	void Start () {
        handsProvider = Object.FindObjectOfType<HandsProvider>();
        this.isGazed = false;
	}
	

	void Update () {
        if (IsGrabbed())
        {
            SetGrabMaterial();
        } else if (IsNearHand())
        {
            SetNearMaterial();
            gameObject.GetComponentInChildren<Text>().enabled = true;
        } else if(isGazed)
        {
            gameObject.GetComponentInChildren<Text>().enabled = true;
            SetDefaultMaterial();
        } else
        {
            gameObject.GetComponentInChildren<Text>().enabled = false;
            SetDefaultMaterial();
        }

        //gameObject.GetComponentInChildren<Text>().transform.LookAt(Camera.main.transform);

    }

    /// <summary>
    /// true if the object is grabbed.
    /// </summary>
    /// <returns></returns>
    private bool IsGrabbed()
    {
        return gameObject.GetComponent<GrabInteraction>() != null 
            && gameObject.GetComponent<GrabInteraction>().State.Equals(InteractionState.On);
    }

    /// <summary>
    /// true if the object is near to some hand.
    /// </summary>
    /// <returns></returns>
    private bool IsNearHand()
    {
        var f = false;
        foreach (var hand in handsProvider.ActiveHands)
        {
            if (hand.Palm.NearObjects.Contains(gameObject.GetComponent<GrabInteraction>()))
            {
                f = true;
                break;
            }
        }
        return f;
    }

    public void SetDefaultMaterial()
    {
        SetMaterial(_default);
    }

    public void SetNearMaterial()
    {
        SetMaterial(_near);
    }   

    public void SetGrabMaterial()
    {
        SetMaterial(_grab);
    }

    public void OnGazeStart()
    {
        this.isGazed = true;
    }

    public void OnGazeEnd()
    {
        this.isGazed = false;
    }

    private void SetMaterial(Material m)
    {
        gameObject.GetComponent<Renderer>().material = m;
    }


}
