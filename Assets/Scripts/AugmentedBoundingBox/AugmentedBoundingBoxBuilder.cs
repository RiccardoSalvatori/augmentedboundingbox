﻿using System.Collections.Generic;
using UnityEngine;
using Meta;
using Meta.HandInput;
using System;
using UnityEngine.UI;
using System.Linq;
using Assets.Scripts.AugmentedBoundingBox;

/// <summary>
/// Bounding box builder
/// </summary>
class AugmentedBoundingBoxBuilder
{
    private const string OBJECT_TAG = "BoundingBox";
    private const string DEFAULT_DESC = "Object";
    private List<Vector3> verts;
    private string description;
    private PrimitiveType shape;
    private List<BoundingBoxType> types;

    public AugmentedBoundingBoxBuilder()
    {
        Reset();
    }

    private void Reset()
    {
        verts = new List<Vector3>();
        description = DEFAULT_DESC;
        shape = PrimitiveType.Cube;
        types = new List<BoundingBoxType>();
        types.Add(BoundingBoxType.Grabbable);
    }

    public AugmentedBoundingBoxBuilder Dimensions(List<Vector3> dimensions)
    {
        this.verts = new List<Vector3>(dimensions);
        return this;
    }

    public AugmentedBoundingBoxBuilder Shape(PrimitiveType shape)
    {
        this.shape = shape;
        return this;
    }

    public AugmentedBoundingBoxBuilder Types(List<BoundingBoxType> types)
    {
        if (types.Any())
        {
            this.types = new List<BoundingBoxType>(types);
        }
        
        return this;
    }

    //Build a bounding box that can be grabbed and moved.
    public AugmentedBoundingBoxBuilder Grabbable()
    {
        types.Add(BoundingBoxType.Grabbable);
        return this;

    }

    //Build a bounding box that works as a trigger area.
    public AugmentedBoundingBoxBuilder Triggerer()
    {
        types.Add(BoundingBoxType.Triggerer);
        return this;

    }

    public AugmentedBoundingBoxBuilder Rotatable()
    {
        types.Add(BoundingBoxType.Rotatable);
        return this;
    }

    public AugmentedBoundingBoxBuilder Description(string description)
    {
        if(!String.IsNullOrEmpty(description))
            this.description = description;
        return this;

    }

    public IAugmentedBoundingBox Build()
    {
        GameObject prefab;
        switch (shape)
        {
            case PrimitiveType.Cube:
                prefab = Resources.Load<GameObject>("bbPrefab/Cube");
                break;
            case PrimitiveType.Cylinder:
                prefab = Resources.Load<GameObject>("bbPrefab/Cylinder");
                break;

            case PrimitiveType.Sphere:
                prefab = Resources.Load<GameObject>("bbPrefab/Sphere");
                break;
            case PrimitiveType.Capsule:
                prefab = Resources.Load<GameObject>("bbPrefab/Capsule");
                break;
            default:
                prefab = Resources.Load<GameObject>("bbPrefab/Cube");
                break;
        }

        GameObject boundingBox = GenerateBoundingBox(verts.ToArray(),prefab);
        boundingBox.GetComponentInChildren<Text>().text = this.description;

        IAugmentedBoundingBox decorated = 
            new AugmentedBoundingBox(verts, description, shape, boundingBox);

        if (types.Contains(BoundingBoxType.Grabbable))
        {
            decorated = new Grabbable(decorated);
        }

        if (types.Contains(BoundingBoxType.Triggerer))
        {
            decorated = new Triggerer(decorated);
        }

        if (types.Contains(BoundingBoxType.Rotatable))
        {
            decorated = new Rotatable(decorated);
        }

        if (types.Contains(BoundingBoxType.Scalable))
        {
            decorated = new Scalable(decorated);
        }

        if (types.Contains(BoundingBoxType.Gazeable))
        {
            decorated = new Gazeable(decorated);
        }
        Reset();

        return decorated;
    }

    /// <summary>
    /// Generates a cube from the given 4 verteces array that specifies dimensions. The first and the second vertex 
    /// determines the height, the second and the third the width, the third and the fourth the depth.
    /// </summary>
    /// <param name="verts">array of four vertex</param>
    /// <param name="_prefab">prefab to instanciate</param>
    /// <returns>The generated prefab</returns>
    private GameObject GenerateBoundingBox(Vector3[] verts, GameObject _prefab)
    {
        GameObject boundingBox = GameObject.Instantiate(_prefab);
        boundingBox.tag = OBJECT_TAG;
        boundingBox.name = this.description;
        //_object.layer = 15;//Invisible layer in webcam

        var h = Vector3.Distance(verts[0], verts[1]);
        var w = Vector3.Distance(verts[1], verts[2]);
        var d = Vector3.Distance(verts[2], verts[3]);
        boundingBox.transform.localScale = new Vector3(w, h, d);

        //this should give the center of the object 
        boundingBox.transform.position = verts[0] + new Vector3(w / 2, h / 2, d / 2);

        boundingBox.GetComponentInChildren<BoundingBoxVisualFeedback>().SetDefaultMaterial();

        return boundingBox;
    }





}
