﻿
using Assets.Scripts.AugmentedBoundingBox;
using Meta;
using UnityEngine;

public enum BoundingBoxType
{
    /// <summary>
    /// 
    /// </summary>
    Grabbable,
    /// <summary>
    /// 
    /// </summary>
    Triggerer,
    /// <summary>
    /// 
    /// </summary>
    Rotatable,
    /// <summary>
    /// 
    /// </summary>
    Scalable,
    /// <summary>
    /// 
    ///  </summary>
    Gazeable

}

class Grabbable : BoundingBoxDecorator
{
    
    public Grabbable(IAugmentedBoundingBox augBB) : base(augBB)
    {
        var obj = this.GetObject();
        var _rigidBody = obj.AddComponent<Rigidbody>();
        _rigidBody.useGravity = false;
        _rigidBody.isKinematic = true;

        var grabInteraction = obj.AddComponent<GrabInteraction>();

        base.AddType(BoundingBoxType.Grabbable);
    }
}
class Triggerer : BoundingBoxDecorator
{

    public Triggerer(IAugmentedBoundingBox augBB) : base(augBB)
    {
        Collider collider = this.augBB.GetObject().GetComponent<Collider>();
        if (collider == null)
        {
            this.augBB.GetObject().AddComponent<Collider>();
        }
        collider.isTrigger = true;
        base.augBB.GetObject().AddComponent<HoverInteraction>();

        base.AddType(BoundingBoxType.Triggerer);
    }

}
class Rotatable : BoundingBoxDecorator
{

    public Rotatable(IAugmentedBoundingBox augBB) : base(augBB)
    {
        base.augBB.GetObject().AddComponent<TwoHandGrabRotateInteraction>();
        base.AddType(BoundingBoxType.Rotatable);
    }
}
class Scalable : BoundingBoxDecorator
{

    public Scalable(IAugmentedBoundingBox augBB) : base(augBB)
    {
        base.augBB.GetObject().AddComponent<TwoHandScaleInteraction>();
        base.AddType(BoundingBoxType.Scalable);
    }
}
class Gazeable : BoundingBoxDecorator, IGazeEndEvent, IGazeStartEvent
{

    public Gazeable(IAugmentedBoundingBox augBB) :  base(augBB)
    {
        Collider collider = this.augBB.GetObject().GetComponent<Collider>();
        if (collider == null)
        {
            this.augBB.GetObject().AddComponent<Collider>();
        }
        collider.isTrigger = true;

        base.AddType(BoundingBoxType.Scalable);
    }


    public void OnGazeEnd()
    {
        throw new System.NotImplementedException();
    }

    public void OnGazeStart()
    {
        throw new System.NotImplementedException();
    }
}