﻿using Meta;
using Meta.HandInput;
using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using Assets.Scripts.AugmentedBoundingBox;

class AugmentedBoundingBox : IAugmentedBoundingBox
{

    private const float SCALE_FACTOR = 1.2f;

    /// <summary>
    /// 4 verteces specifing dimensions height, width, depth
    /// </summary>
    private List<Vector3> dimensions;
    private HashSet<BoundingBoxType> types;
    private PrimitiveType shape;
    private string description;

    public GameObject _object { get; private set; }


    public AugmentedBoundingBox(List<Vector3> dimensions, 
        string description,
        PrimitiveType shape,
        GameObject _object)
    {
        this.dimensions = new List<Vector3>(dimensions);
        this.description = description;
        this.shape = shape;
        this._object = _object;
        this.types = new HashSet<BoundingBoxType>();
        
    }
    public void IncreaseScale()
    {
        _object.transform.localScale *= SCALE_FACTOR;
    }

    public void DecreaseScale()
    {
        _object.transform.localScale /= SCALE_FACTOR;
    }

    public BoundingBoxData GetSerializable()
    {
        return new BoundingBoxData(
            this._object.GetComponent<Transform>(),
            this.dimensions,
            this.types.Select(t => t.ToString()).ToList(),
            this.description,
            this.shape.ToString()
            );
    }

    public override string ToString()
    {
        string vertsString = "VERTS\n";
        foreach(Vector3 v in dimensions)
        {
            vertsString += v.ToString()+"\n";
        }

        string typesString = "TYPES\n";
        foreach(string t in this.types.Select(t => t.ToString()).ToList())
        {
            typesString += t+"\n";
        }

        string descString = "DESC\n"+description;

        return vertsString + typesString + descString;
    }

    public GameObject GetObject()
    {
        return this._object;
    }

    public void AddType(BoundingBoxType t)
    {
        this.types.Add(t);
    }
}
