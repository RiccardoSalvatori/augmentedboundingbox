﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.AugmentedBoundingBox
{
    interface IAugmentedBoundingBox
    {
        GameObject GetObject();

        void IncreaseScale();

        void DecreaseScale();

        BoundingBoxData GetSerializable();

        void AddType(BoundingBoxType type);
    }
}
