﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System;
using Meta;
using Assets.Scripts.AugmentedBoundingBox;

class EnvironmentManager
{
    
    private char pathSeparator = Path.DirectorySeparatorChar;
    /// <summary>
    /// Directory containing environmnets
    /// </summary>
    private string ENVS_DIR = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) 
        + Path.DirectorySeparatorChar + "env";

    private List<IAugmentedBoundingBox> bbList;

    /// <summary>
    /// Creates a new directory at the specified path.
    /// If the directory already exists is overwritten.
    /// </summary>
    /// <param name="path"></param>
    private void CreateDirectory(string path)
    {
        try
        {
            // Determine whether the directory exists.
            if (Directory.Exists(path))
            {
                //Directory.Delete(path);
            }
            else
            {
                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(path);
                Debug.Log("The directory was created successfully at." +
                    Directory.GetCreationTime(path) + " " + path);
            }
        }
        catch (Exception)
        {
            Debug.Log("The process failed");
        }
        finally { }

    }

    public EnvironmentManager()
    {
        bbList = new List<IAugmentedBoundingBox>();
        CreateDirectory(ENVS_DIR);
    }

    public void AddBoundingBox(IAugmentedBoundingBox b)
    {
        this.bbList.Add(b);
    }

    public void RemoveLastBoundingBox()
    {
        int last = bbList.Count - 1;
        UnityEngine.Object.Destroy(bbList[last].GetObject());
        this.bbList.RemoveAt(last);
    }

    public IAugmentedBoundingBox GetLastBoundingBox()
    {
        return this.bbList[bbList.Count - 1];
    }
    /// <summary>
    /// Save the list of bounding box in a directory with the given name. 
    /// If the directory exsists is overwritten.
    /// </summary>
    /// <param name="bbList"></param>
    /// <param name="envName"></param>
    public void SaveEnvironment(string envName)
    {
        string path = ENVS_DIR + pathSeparator + envName;
        if (Directory.Exists(path))
        {
            foreach(var file in Directory.GetFiles(path))
            {
                File.Delete(file);
            }
            Directory.Delete(path);
        }
        CreateDirectory(path);
        foreach (AugmentedBoundingBox b in this.bbList)
        {
            SaveBoundingBox(b, envName);
        }
    }

    /// <summary>
    /// Save a bounding box in the given environment.
    /// IF the object already exists is overwritten.
    /// </summary>
    /// <param name="bb"></param>
    /// <param name="envName"></param>
    public void SaveBoundingBox(AugmentedBoundingBox bb, string envName)
    {
        string name = bb._object.name +  bb._object.GetInstanceID().ToString();
        string destination = ENVS_DIR + pathSeparator + envName + pathSeparator + name + ".txt";
        
        if (File.Exists(destination))
        {
            File.Delete(destination);
        }

        FileStream file = File.Create(destination);

        byte[] toWrite = Encoding.ASCII.GetBytes(Serialize(bb));
        file.Write(toWrite, 0, toWrite.Length);
        file.Close();
    }

    public List<IAugmentedBoundingBox> GetCurrentEnvironmnet()
    {
        return new List<IAugmentedBoundingBox>(this.bbList);
    }


 

    public List<IAugmentedBoundingBox> LoadEnvironment(string envName)
    {
        var path = ENVS_DIR + pathSeparator + envName;
        this.bbList = new List<IAugmentedBoundingBox>();
        DirectoryInfo dir = new DirectoryInfo(path);

        foreach (FileInfo file in dir.GetFiles("*.txt"))
        {
            string json = File.ReadAllText(file.FullName);

            bbList.Add(Deserialize(json));
        }

        UnityEngine.Object.FindObjectOfType<MetaReconstruction>().LoadReconstruction("1");
        return new List<IAugmentedBoundingBox>(this.bbList);
    }

    private string Serialize(IAugmentedBoundingBox boundigBox)
    {
        return JsonUtility.ToJson(boundigBox.GetSerializable());
    }

    public List<string> GetAllEnvironments()
    {
        List<string> toReturn = new List<string>();
        foreach(DirectoryInfo d in new DirectoryInfo(ENVS_DIR).GetDirectories())
        {
            toReturn.Add(d.Name);
        }
        return toReturn;
    }

    private IAugmentedBoundingBox Deserialize(string json)
    {
        BoundingBoxData data = JsonUtility.FromJson<BoundingBoxData>(json);

        List<BoundingBoxType> types = data.types
            .Select(t => (BoundingBoxType)Enum.Parse(typeof(BoundingBoxType), t))
            .ToList();
        PrimitiveType shape = (PrimitiveType)Enum.Parse(typeof(PrimitiveType), data.shape);

        IAugmentedBoundingBox boundingBox =
            new AugmentedBoundingBoxBuilder()
            .Dimensions(data.vertices)
            .Description(data.description)
            .Types(types)
            .Shape(shape)
            .Build();

        Transform transform = boundingBox.GetObject().GetComponent<Transform>();
        transform.position = data.position;
        transform.rotation = data.rotation;
        transform.localScale = data.scale;

        return boundingBox;
    }

}

/// <summary>
/// Keep track of bounding box informations that need to be serialized.
/// </summary>
[Serializable]
public class BoundingBoxData
{
    public List<Vector3> vertices;
    public List<string> types;
    public string description;
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 scale;
    public string shape;

    public BoundingBoxData(Transform t, 
        List<Vector3> vertices, 
        List<string> types, 
        string description,
        string shape)
    {
        this.vertices = vertices;
        this.types = types;
        this.description = description;
        this.position = t.position;
        this.rotation = t.rotation;
        this.scale = t.localScale;
        this.shape = shape;
    }
}