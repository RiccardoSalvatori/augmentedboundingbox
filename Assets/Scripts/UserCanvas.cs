﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserCanvas : MonoBehaviour {

    [SerializeField]
    private GameObject buttonPrefab;
    private EnvironmentManager envManager;

	// Use this for initialization
	void Start () {
        envManager = new EnvironmentManager();
        var envList = GameObject.Find("EnvList").GetComponent<Transform>();
        foreach (string envName in envManager.GetAllEnvironments())
        {
            var b = Instantiate(buttonPrefab, envList);
            b.GetComponentInChildren<Text>().text = envName;
            b.GetComponent<Button>().onClick.AddListener(() =>
            {
                envManager.LoadEnvironment(b.GetComponentInChildren<Text>().text);
                Destroy(this.gameObject);
            });
        }
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
