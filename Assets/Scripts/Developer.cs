﻿using Assets.Scripts.AugmentedBoundingBox;
using UnityEngine;

/// <summary>
/// Handles creation of bounding box.
/// </summary>
public class Creator : MonoBehaviour
{
    private RayVision rayVision;
    private EnvironmentManager environmentManager;
    private Palette palette;
    private CreatorCanvas creatorCanvas;

    [SerializeField]
    private GameObject shapePalette;

    void Start()
    {
        rayVision = gameObject.GetComponent<RayVision>();
        palette = shapePalette.GetComponent<Palette>();
        environmentManager = new EnvironmentManager();
        creatorCanvas = GameObject.Find("CreatorCanvas").GetComponent<CreatorCanvas>();
        creatorCanvas.SaveButtonClick += (s,e) =>
        {
            environmentManager.SaveEnvironment(creatorCanvas.GetEnvironmentName());
        };
    }

    void Update()
    {
        rayVision.Cast();

        //Create a bounding box 
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            IAugmentedBoundingBox bb =
                new AugmentedBoundingBoxBuilder()
                .Dimensions(rayVision.GetSavedVerts())
                .Shape(palette.GetSelectedPrimitiveType())
                .Types(creatorCanvas.GetSelectedTypes())
                .Description(creatorCanvas.GetObjectDescription())
                .Build();

            rayVision.DestroySavedVertices();
            environmentManager.AddBoundingBox(bb);
        }

        //Remove last created bounding box
        if (Input.GetKeyDown(KeyCode.D))
        {
            environmentManager.RemoveLastBoundingBox();
        }

        //Click "1" to increase size of the last bounding box
        if (Input.GetKeyDown(KeyCode.Plus))
        {
            environmentManager.GetLastBoundingBox().IncreaseScale();
        }

        //Click "2" to decrease size of the last bounding box
        if (Input.GetKeyDown(KeyCode.Minus))
        {
            environmentManager.GetLastBoundingBox().DecreaseScale();
        }

        //Add vertex
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rayVision.SaveWatchingVertex();           
        }

        //Remove the last vertex if "canc" is pressed
        if (Input.GetKeyDown(KeyCode.Delete))
        {
            rayVision.RemoveLastVertex();
        }
    }

    private void OnApplicationQuit()
    {
        this.environmentManager.SaveEnvironment(this.creatorCanvas.GetEnvironmentName());
    }



    //DEMO
    /*private void SetGrabInteractionEvents(GameObject bb)
    {
        bb.GetComponent<GrabInteraction>().Events.Engaged.AddListener(e =>
        {

            var cube1 = GameObject.Find("MetaCube");
            var cube2 = GameObject.Find("MetaCube2");

            cube1.GetComponent<GrabInteraction>().enabled = false;
            cube2.GetComponent<GrabInteraction>().enabled = false;
        });

        bb.GetComponent<GrabInteraction>().Events.Disengaged.AddListener(e =>
        {

            var cube1 = GameObject.Find("MetaCube");
            var cube2 = GameObject.Find("MetaCube2");

            cube1.GetComponent<GrabInteraction>().enabled = true;
            cube2.GetComponent<GrabInteraction>().enabled = true;
        });
    }

    private void SetTriggerEvents(GameObject bb)
    {
        bb.GetComponent<HoverInteraction>().Events.HoverStart.AddListener(e =>
        {
            var cube1 = GameObject.Find("MetaCube");
            var cube2 = GameObject.Find("MetaCube2");

            cube1.GetComponent<Rigidbody>().useGravity = true;
            cube2.GetComponent<Rigidbody>().useGravity = true;

        });
    }*/


}

