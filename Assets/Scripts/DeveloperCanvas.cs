﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatorCanvas : MonoBehaviour {

    private List<BoundingBoxType> types;
    private Vector3 offset;

    public event EventHandler SaveButtonClick;
    

	// Use this for initialization
	void Start () {
        this.offset = this.gameObject.transform.position;
        types = new List<BoundingBoxType>();
        Button saveButton = this.GetComponentInChildren<Button>();
        saveButton.interactable = false;
        saveButton.onClick.AddListener(() => 
            {
                SaveButtonClick.Invoke(this, EventArgs.Empty);
            });

       foreach(Toggle t in GetComponentsInChildren<Toggle>())
        {
            t.onValueChanged.AddListener((e) =>
            {
                string label = t.GetComponentInChildren<Text>().text;
                BoundingBoxType type = (BoundingBoxType)Enum.Parse(typeof(BoundingBoxType), label);
                if (t.isOn)
                {
                    this.types.Add(type);
                    
                } else
                {
                    this.types.Remove(type);
                }
            });
        }
	}

    public string GetObjectDescription()
    {
        return GameObject.Find("InputField_ObjDesc").GetComponentInChildren<Text>().text;
    }

    public List<BoundingBoxType> GetSelectedTypes()
    {
        return new List<BoundingBoxType>(this.types);
    }

    public string GetEnvironmentName()
    {
        return GameObject.Find("InputField_EnvName").GetComponentInChildren<Text>().text;
    }
	
	// Update is called once per frame
	void Update () {
        //Move canvas with user
        this.gameObject.transform.position = Camera.main.transform.position + offset;

        if(GameObject.FindGameObjectsWithTag("BoundingBox").Length > 0 
            && !String.IsNullOrEmpty(GetEnvironmentName()))
        {
            this.GetComponentInChildren<Button>().interactable = true;
        }
		
	}
}
