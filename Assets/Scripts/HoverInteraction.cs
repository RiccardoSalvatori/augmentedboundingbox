﻿using UnityEngine;
using UnityEditor;
using Meta;
using Meta.HandInput;

/// <summary>
/// Interaction that triggers only on hovers events.  
/// </summary>
public class HoverInteraction : Interaction
{
    private HandFeature _handFeature;

    protected override bool CanEngage(Hand handProxy)
    {
        return false;
    }

    protected override void Engage()
    {
    }

    protected override bool CanDisengage(Hand handProxy)
    {
        return false;
    }

    protected override void Disengage()
    {
    }

    protected override void Manipulate()
    {
    }
}

