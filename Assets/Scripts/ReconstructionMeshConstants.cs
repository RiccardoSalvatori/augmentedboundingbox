﻿/**
 * Constants of the reconstruction mesh. 
 */
public static class ReconstructionMeshConstants
{
    //Names given by Meta to the generated reconstruction
    public const string RECONS_OBJ = "Reconstruction";
    public const string LOADED_RECONS_OBJ = "LoadedReconstruction";

    //Layer containing only the reconstruction mesh
    public static int RECONS_MESH_LAYER_IDX = 10;
    public static string RECONS_MESH_LAYER_NAME = "worldMesh";

}