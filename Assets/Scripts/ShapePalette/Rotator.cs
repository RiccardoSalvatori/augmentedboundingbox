﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta;

/**
 * Script to rotate an object.
 */
public class Rotator : MonoBehaviour {


    private Vector3 rotationVector;
    public bool _stop;
    public float x = 0;
    public float y = 20;
    public float z = 0;

    void Start()
    {
        _stop = false;
        rotationVector = new Vector3(x,y,z);
    }

    // Update is called once per frame
    void Update()
    {
        rotationVector = new Vector3(x, y, z);
        if (!_stop)
            transform.Rotate(rotationVector * Time.deltaTime);
    }

}
