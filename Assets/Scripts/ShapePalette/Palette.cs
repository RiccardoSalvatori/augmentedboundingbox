﻿using Meta;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Palette : MonoBehaviour
{

    private List<Shape> shapes;
    private Shape selected;
    private GrabInteraction _garbInteraction;
    private Vector3 prevUserPosition;

    [SerializeField]
    private Material focusMaterial;

    /*[SerializeField]
    private GameObject hud;*/

    // Use this for initialization
    void Start()
    {
        shapes = new List<Shape>();
        foreach (Transform child in transform)
        {
            var materialchanger = new MaterialChanger(focusMaterial,
                                            child.gameObject.GetComponent<Renderer>());
            shapes.Add(new Shape(child.gameObject, materialchanger));
        }

        selected = shapes[0];
        selected.Focus();
        _garbInteraction = gameObject.GetComponent<GrabInteraction>();
        _garbInteraction.Events.Engaged.AddListener(e =>
        {
            foreach(Shape s in shapes)
            {
                s.Unfocus();
            }
        });

        _garbInteraction.Events.Disengaged.AddListener(e =>
        {
            selected.Focus();
        });

        //hud.GetComponentInChildren<TemporalHelpAnimationMessageController>().Show();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var shape in shapes)
        {
            if (shape.IsNearHand())
            {
                shape.Focus();
                selected = shape;
                //hud.GetComponentInChildren<Text>().text = selected.gameObject.name;
            }
            else
            {
                if (!shape.Equals(selected))
                {
                    shape.Unfocus();
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            int index = mod(shapes.IndexOf(selected) + 1, shapes.Count);
            selected.Unfocus();
            selected = shapes[index];
            selected.Focus();
            //hud.GetComponentInChildren<Text>().text = selected.gameObject.name;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            int index = mod(shapes.IndexOf(selected) - 1, shapes.Count);
            selected.Unfocus();
            selected = shapes[index];
            selected.Focus();
            //hud.GetComponentInChildren<Text>().text = selected.gameObject.name;
        }
    }

    private int mod(int x, int m)
    {
        return (x % m + m) % m;
    }

    private Vector3 CurrentPosition()
    {
        return gameObject.transform.localPosition;
    }

    public PrimitiveType GetSelectedPrimitiveType()
    {
        switch (selected._gameObject.name)
        {
            case "Cube": return PrimitiveType.Cube;
            case "Sphere": return PrimitiveType.Sphere;
            case "Cylinder": return PrimitiveType.Cylinder;
            case "Capsule": return PrimitiveType.Capsule;
            default: return PrimitiveType.Cube;
        }
    }
}
