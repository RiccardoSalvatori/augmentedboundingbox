﻿using UnityEngine;
using System.Collections;
using Meta;
using System.Collections.Generic;

public class Shape
{
    public GameObject _gameObject { get; private set; }
    private MaterialChanger materialChanger;
    private List<Light> focuslight;
    private Floater _floater;
    private HandsProvider handsProvider;

    public Shape(GameObject gameObject, MaterialChanger materialChanger)
    {
        this._gameObject = gameObject;
        this.materialChanger = materialChanger;
        this.focuslight = new List<Light>(gameObject.GetComponentsInChildren<Light>()) ;
        this.handsProvider = Object.FindObjectOfType<HandsProvider>();
        this._floater = _gameObject.GetComponent<Floater>();

    }

    public void Focus()
    {
        this.materialChanger.ChangeMaterial();
        this.focuslight.ForEach(l => l.enabled = true);
        this._floater.StartFloating();
    }

    public void Unfocus()
    {
        this.materialChanger.StartMaterial();
        this.focuslight.ForEach(l => l.enabled = false);
        this._floater.StopFloating();
    }

    public bool IsNearHand()
    {
        var f = false;
        foreach (var hand in handsProvider.ActiveHands)
        {
            if (hand.Palm.NearObjects.Contains(this._gameObject.GetComponent<HoverInteraction>()))
            {
                f = true;
                break;
            }
        }
        return f;
    }
}
