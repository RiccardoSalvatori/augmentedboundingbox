﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

/// <summary>
/// Simple class that makes an object change material.
/// </summary>
public class MaterialChanger
{
    private List<Material> startMaterials;
    private List<Material> endMaterials;
    private Renderer renderer;

    public MaterialChanger(List<Material> endMaterials, Renderer renderer)
    {
        this.renderer = renderer;
        this.startMaterials = new List<Material>(renderer.materials);
        this.endMaterials = endMaterials;
    }

    public MaterialChanger(Material endMaterial, Renderer renderer)
    {
        this.renderer = renderer;
        this.startMaterials = new List<Material>(renderer.materials);
        this.endMaterials = new List<Material>(new Material[] { endMaterial });
    }

    public void ChangeMaterial()
    {
        SetMaterials(endMaterials);
    }

    public void StartMaterial()
    {
        SetMaterials(startMaterials);
    }

    private void SetMaterials(List<Material> newMaterials)
    {
        Material[] mats = renderer.materials;
        for (int i = 0; i < mats.Length; i++)
        {
            mats[i] = newMaterials[i];
        }
        renderer.materials = mats;
    }
}