﻿using Meta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Cast multiple rays.
/// </summary>
class MultipleRays : AbstractVisionStrategy
{
    private int rows;
    private int raysPerRow;
    private float theta;

    public MultipleRays(SceneDrawer drawer) : base(drawer)
    {
        rows = 5;
        raysPerRow = 5;
        theta = 2;
    }

    public MultipleRays(SceneDrawer drawer, int rows, int raysPerRow, float theta) : base(drawer)
    {
        this.rows = rows;
        this.raysPerRow = raysPerRow;
        this.theta = theta;
    }

    public override Vector3 CastVision(Transform t)
    {
        int n = ReconstructionMeshConstants.RECONS_MESH_LAYER_IDX;
        var layerMask = 1 << n;//Check collisions only with the world mesh layer
        List<RaycastHit> hits = MultiRaycast.MultiRayCast(t.position,
                                                      t.forward,
                                                      rows,
                                                      raysPerRow,
                                                      theta,
                                                      ~layerMask).ToList();//MultiRaycast ignores colliders in the given layer mask, 
                                                                           //I want to ignore all but thy given layer.

        MeshCollider meshCollider = null;
        if (hits.Any())
        {
            hits.ForEach(hit =>
            {
                meshCollider = hit.collider as MeshCollider;
                if (ExistsCollisionMesh(meshCollider))
                {
                    DrawHitTriangle(meshCollider.sharedMesh, hit, GetColor());
                }
            });


            var mainHit = hits[0];
            var mc = mainHit.collider as MeshCollider;

            if (ExistsCollisionMesh(mc))
            {
                DrawHitTriangle(mc.sharedMesh, mainHit, Color.red);
                return GetHitTriangleVerteces(mainHit)[0];
            } else
            {
                return Vector3.zero;
            }
        }
        else
        {
            return Vector3.zero;
        }

    }


}

