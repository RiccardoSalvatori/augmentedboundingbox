﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Define commons functionality between visions
/// </summary>
public abstract class AbstractVisionStrategy : IVisionStrategy
{
    protected SceneDrawer drawer;

    public AbstractVisionStrategy(SceneDrawer drawer)
    {
        this.drawer = drawer;
    }

    public abstract Vector3 CastVision(Transform t);

    protected bool ExistsCollisionMesh(MeshCollider meshCollider)
    {
        return !(meshCollider == null || meshCollider.sharedMesh == null);
    }

    /// <summary>
    /// Return the hitted triangles vertices by the given raycast hit
    /// </summary>
    /// <param name="hit"></param>
    /// <returns></returns>
    protected Vector3[] GetHitTriangleVerteces(RaycastHit hit)
    {
        MeshCollider m = hit.collider as MeshCollider;
        Vector3[] vertices = m.sharedMesh.vertices;
        int[] triangles = m.sharedMesh.triangles;

        Vector3[] toReturn = new Vector3[3];
        var idx = hit.triangleIndex * 3;
        toReturn[0] = vertices[triangles[idx]];
        toReturn[1] = vertices[triangles[idx+1]];
        toReturn[2] = vertices[triangles[idx+2]];
        return toReturn;
    }

    protected void DrawHitTriangle(Mesh mesh, RaycastHit hit, Color c)
    {
        int n = 3;
        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;

        Vector3[] points = new Vector3[n];
        Transform hitTransform = hit.collider.transform;


        for (int i = 0; i < n; i++)
        {
            int t_index = hit.triangleIndex * n + i;
            if (t_index <= triangles.Length)
            {
                points[i] = vertices[triangles[t_index]];
                points[i] = hitTransform.TransformPoint(points[i]);
            }
        }


        drawer.DrawPolygon(points, c);
    }

    protected Color GetColor()
    {
        return new Color(UnityEngine.Random.Range(0.1f, 0.2f),  //r
                         UnityEngine.Random.Range(0.1f, 0.2f),  //g
                          UnityEngine.Random.Range(0.8f, 1.0f)); //b
    }

}