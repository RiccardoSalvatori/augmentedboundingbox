﻿using UnityEngine;

/// <summary>
/// Interface for user simulated vision
/// </summary>
public interface IVisionStrategy
{
    /// <summary>
    /// Cast vision and return hitted point
    /// </summary>
    /// <param name="t">The point where to start vision</param>
    /// <returns></returns>
    Vector3 CastVision(Transform t);
}
