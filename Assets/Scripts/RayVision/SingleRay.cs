﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Cast a single ray.
/// </summary>
internal class SingleRay : AbstractVisionStrategy
{
    public SingleRay(SceneDrawer drawer) : base(drawer)
    {
    }

    public override Vector3 CastVision(Transform t)
    {
        int n = ReconstructionMeshConstants.RECONS_MESH_LAYER_IDX;
        var layerMask = 1 << n;//Check only collsion with world mesh layer
        RaycastHit hit;
        var ray = new Ray(t.position, t.forward);
        if (!Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            return Vector3.zero;
        }
        else
        {
            MeshCollider meshCollider = hit.collider as MeshCollider;
            if (ExistsCollisionMesh(meshCollider))
            {

                DrawHitTriangle(meshCollider.sharedMesh, hit, GetColor());

                return GetHitTriangleVerteces(hit)[0];
            }
            else
            {
                return Vector3.zero;
            }
        }
    }

}
