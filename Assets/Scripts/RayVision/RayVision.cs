﻿using System.Collections.Generic;
using UnityEngine;
using Meta;
using System.Linq;
using System.Threading;

/// <summary>
/// Simulate eye vision with raycasting.
/// Handle the interaction between the world mesh generated by Meta and the user gaze.
/// This script is placed in the MetaCameraRig. 
/// Provide both single and multiple rays strategy.
/// </summary>
class RayVision : MonoBehaviour
{

    public Vector3 WatchingVertex { get; private set; }
    public List<GameObject> VertsObj { get; private set; }

    private Camera cam;

    [SerializeField]
    private bool multi = false;
    [SerializeField]
    private float visionTime = 0.8f;
    [SerializeField]
    private GameObject linePrefab;
    [SerializeField]
    private GameObject pntPrefab;

    private IVisionStrategy singleRay;
    private IVisionStrategy multipleRays;


    private SceneDrawer sceneDrawer;

    private Mesh worldMesh;

    private void Start()
    {
        cam = Camera.main;

        VertsObj = new List<GameObject>();
        WatchingVertex = Vector3.zero;
        sceneDrawer = new SceneDrawer(linePrefab, pntPrefab, visionTime);
        singleRay = new SingleRay(sceneDrawer);
        multipleRays = new MultipleRays(sceneDrawer);
    }


    /// <summary>
    /// Returns a list of the saved vertices position
    /// </summary>
    /// <returns></returns>
    public List<Vector3> GetSavedVerts()
    {
        return VertsObj.Select(v => v.transform.position).ToList();
    }

    public void Cast()
    {
        if (multi)
        {
            WatchingVertex = multipleRays.CastVision(cam.transform);
        }
        else
        {
            WatchingVertex = singleRay.CastVision(cam.transform);
        }
    }

    public void SaveWatchingVertex()
    {
        VertsObj.Add(Instantiate(pntPrefab, WatchingVertex, Quaternion.identity));
    }

    public void RemoveLastVertex()
    {
        if (VertsObj.Any())
        {
            Destroy(VertsObj.Last());
            VertsObj.RemoveAt(VertsObj.Count - 1);
        }
    }

    public void DestroySavedVertices()
    {
        foreach (var obj in VertsObj)
        {
            Destroy(obj);
        }
        VertsObj = new List<GameObject>();
    }

}









