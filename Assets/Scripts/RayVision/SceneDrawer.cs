﻿using System.Linq;
using UnityEngine;

/// <summary>
///  Provides functionality to draw shapes in the scene.
///  e.g lines, points, triangles...
/// </summary>
public class SceneDrawer {

    private GameObject linePrefab;
    private GameObject pntPrefab;

    private GameObject linesContainer;
    private Material lineMaterial;
    private float visionTime;

    //visionTime indicates how long the visualization should remain active
    public SceneDrawer(GameObject linePrefab, GameObject pntPrefab, float visionTime)
    {
        this.linePrefab = linePrefab;
        this.pntPrefab = pntPrefab;
        linesContainer = new GameObject("SceneDrawer");
        lineMaterial = new Material(Shader.Find("Standard"));
        this.visionTime = visionTime;
    }

    public void DrawPolygon(Vector3[] points, Color c)
    {
        //points.ToList().ForEach(p => DrawPoint(p));

        for (int i = 0; i < points.Length - 2; i += 3)
        {
            DrawTriangle(points[i], points[i + 1], points[i + 2], c);
        }
    }

    //Draw a small sphere in the given position
    public void DrawPoint(Vector3 pos, Color c, bool _fixed = false)
    {
        GameObject obj = GameObject.Instantiate(pntPrefab,pos,Quaternion.identity);
        obj.GetComponent<Renderer>().material.SetColor("_EmissionColor", c);
        obj.transform.SetParent(linesContainer.transform);
        obj.layer = 5; //Should be UI layer;
        obj.name = c.ToString();

        if (!_fixed)
        {
            UnityEngine.Object.Destroy(obj, visionTime);
        }
        
    }

    //Draw a line between two points.
    public void DrawLine(Vector3 p1, Vector3 p2, Color c, float width = 0.002f)
    {
        var obj = GameObject.Instantiate(linePrefab);
        obj.transform.SetParent(linesContainer.transform);
        obj.layer = 5; //Should be UI layer
        var lr = obj.GetComponent<LineRenderer>();

        lr.material = lineMaterial;
        lr.material.SetColor("_EmissionColor", c);
        lr.material.EnableKeyword("_EMISSION");

        lr.startWidth = width;
        lr.endWidth = width;

        lr.SetPosition(0, p1);
        lr.SetPosition(1, p2);

        UnityEngine.Object.Destroy(obj, visionTime);
    }

    public void DrawTriangle(Vector3 p1, Vector3 p2, Vector3 p3, Color c)
    {
        DrawLine(p1, p2, c);
        DrawLine(p2, p3, c);
        DrawLine(p3, p1, c);
    }
}
